var gulp = require('gulp');
var jade = require('gulp-jade');
var sass = require('gulp-ruby-sass');
var inlineCss = require('gulp-inline-css');
var webserver = require('gulp-webserver');

gulp.task('webserver', function(){
		gulp.src('dist')
			.pipe(webserver({
				livereload: true,
				port: 8039,
				open: true
			}));
});

gulp.task('css', () =>
    sass('./app/sass/*.scss')
        .on('error', sass.logError)
        .pipe(gulp.dest('./dist/css/'))
);
 
gulp.task('html', function() {
  gulp.src('./app/*.jade')
    .pipe(jade({}))
    .pipe(gulp.dest('./dist/'))
});
gulp.task('htmlInlineCSS', function() {
	gulp.src('./dist/*.html')
	.pipe(inlineCss())
   .pipe(gulp.dest('./dist/'))
	
});

gulp.task('watch', function(){
		gulp.watch(['./app/sass/**/*.scss'], ['css'] );
		gulp.watch(['./app/*.jade'], ['html'] );
//		gulp.watch(['./dist/*.html'], ['htmlInlineCSS'] );

});
gulp.task('default', [ 'html', 'css', 'webserver', 'watch' ]);
